# install dependencies:
 npm install
 
# Launch development server, and open `localhost:4200`:
 npm start

# run test:
 npm run test:ci

# Project structure

dist/                                 compiled version
e2e/                                  end-to-end tests
src/                                  project source code
|- app/                               app components
|----/calendar       added the component
|----/cities         development spa
|----/collaboration  development spa
|----/core                             core module (single-use components)
|----/error          development spa
|----/weather        development spa
|- assets/                             app assets ()
|- environments/                       values for various build environments
|- theme/                              app global scss variables and theme
|- index.html                          html entry point
|- main.scss                           global style entry point
|- main.ts                             app entry point
|- polyfills.ts                        polyfills needed by Angular
+- test.ts                             unit tests entry point
reports/                               test and coverage reports
proxy.conf.js                          backend proxy configuration
