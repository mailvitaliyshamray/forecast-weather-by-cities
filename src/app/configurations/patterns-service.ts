import { Injectable } from '@angular/core';
import * as moment from 'moment';

const replaceDailyPropertyName = [
  { day: 'Темп.' },
  { min : 'Т Мин'},
  { max: 'Т Мак' },
  // { night: 'Т ночью' },
  // { eve: 'Т веч.' },
  // { morn: 'Т утром' },
  { speed: 'Ск вет' },
  { clouds: 'Обл' },
  { sunrise: 'Восх' },
  { sunset: 'Закат'},
  { pressure: 'Давл.' },
  { humidity: 'Влаж.'},
  // { deg: 'Напр вет' },
  { dt: 'Дата' }
]; 

export interface SelectedDailyProps {
  dt: string;
  temp: number;
  sunrise: string;
  sunset: string;
  pressure: number;
  humidity: number;
  speed: number;
  clouds: number;
  feels_like: number;
  // deg: number;
  day: number;
  min: number;
  max: number;
  // night: number;
  // eve: number;
  // morn: number;
}

const timstamptDailyParams = ['dt', 'sunrise', 'sunset']
const basicDailyParams = ['pressure', 'humidity', 'speed', 'clouds']
const dailyTemperatureParams = ['day', 'min', 'max']

export interface WeatherItems {
  coord: any;
  weather: any;
  base: string;
  main: object;
  wind: object;
  clouds: object;
  dt: number;
  sys: object;
  timezone: number;
  id: number;
  name: string;
  code: number;
  day: number;
}

@Injectable({
  providedIn: 'root'
})
export class PatternsService {
  cityItems: any;
  deilyProperies: any[];
  allDeilyProperies: any[];

  constructor() {}

  clearDailyProperties() { 
    this.deilyProperies = [];
  }
  
  clearAllDailyProperties() { 
    this.allDeilyProperies = [];
  }

  roundingToNearest(value: any) {
    return typeof value === 'number' ? Math.round(value) : value;
  }

  getDailyTitleItem(propName: string): string {
    const name = replaceDailyPropertyName.find(i => Object.keys(i)[0] === propName);
    return name[propName];
  }

  collectDailyData(value: any, name: string) {
    const prop = {} as SelectedDailyProps;
    prop[name] = timstamptDailyParams.includes(name) ?
      this.transformTimestampToDate(value) : this.roundingToNearest(value);
    const headerName = this.getDailyTitleItem(name);
    const comb = { propName: name, value: prop[name], title: headerName };
    this.deilyProperies.push(comb);
  }

  transformTimestampToDate(timestamp: number) {
    return moment.unix(timestamp).format('YYYY-MM-DD HH:mm')
  }

  separateDailyWeatherData(forecastData: any) { 
    this.clearAllDailyProperties();
    forecastData.list.forEach((data: any, key: number) => {
      this.clearDailyProperties();
      this.handingDailyWeather(data)
      if (this.deilyProperies.length > 1) { 
        this.allDeilyProperies[key] = this.deilyProperies;
      }
    })
  }

  handingDailyWeather(dailyData: any) { 
    for (const [name, value] of Object.entries(dailyData)) {
      if (basicDailyParams.includes(name)) {
        this.collectDailyData(value, name);
      }
      if (timstamptDailyParams.includes(name)) {
        this.collectDailyData(value, name);
      }
      if (name === 'temp' ) { 
        this.handingTempWeather(value)

      }
    }
  }
  
  handingTempWeather(dailyData: any) { 
    for (const [name, value] of Object.entries(dailyData)) {
      if (dailyTemperatureParams.includes(name)) {
        this.collectDailyData(value, name);
      }
    }
  }

  getDailyWeatherProperties(): any[] {
    return this.allDeilyProperies;
  }
}
