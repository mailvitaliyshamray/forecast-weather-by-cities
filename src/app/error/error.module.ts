import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ErrorRoutingModule } from './error-routing.module';
import { ErrorComponent } from './error.component';
  
@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgbModule, ErrorRoutingModule],
  declarations: [ErrorComponent]
})
export class ErrorModule {}
