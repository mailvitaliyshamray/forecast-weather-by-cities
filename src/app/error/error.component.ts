import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '@env/environment';
import { Observable, of } from 'rxjs';
import { SelectedDailyProps, PatternsService } from '@app/configurations/patterns-service';
import { RequestHandingService } from '@app/collaboration/request-handing-service';
import { ParametersProcessingService } from '@app/collaboration/parameters-processing-service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit, OnDestroy {
  
  constructor(
    private requestHandingService: RequestHandingService,
    private parametersProcessingService: ParametersProcessingService,
    private patternsService: PatternsService,
    private router: Router,
    private route: ActivatedRoute,

  ) {

  }
  
  ngOnInit() { }
    
  ngOnDestroy() {}

 
}
