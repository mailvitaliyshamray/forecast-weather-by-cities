import { OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { ParametersProcessingService } from './parameters-processing-service';
import { Logger, untilDestroyed } from '@app/core';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
const log = new Logger('Exchange');

@Injectable({
  providedIn: 'root'
})
export class RequestHandingService implements OnDestroy {
  private startRequest: number;
  private _countRequest: number;
  private error: string | undefined;

  constructor(
    private parametersProcessingService: ParametersProcessingService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnDestroy() {}

  get countRequest(): number {
    return this._countRequest;
  }

  set countRequest(val: number) {
    this._countRequest = val;
  }

  getCountrequest(): Observable<number> {
    this.startRequest = 1;
    const getNumberRequest$ = this.parametersProcessingService.isExpired(this.startRequest);
    getNumberRequest$
      .pipe(
        finalize(() => {
          //
        }),
        untilDestroyed(this),
      )
      .subscribe(
        nextReques => {
          this.countRequest = nextReques.reqCount;
          log.debug(`Left ${nextReques.reqCount} request`);
        },
        error => {
          log.debug(`Ooops request error: ${error}`);
          this.error = error;
        }
      );
    return of(this.countRequest);
  }
}
