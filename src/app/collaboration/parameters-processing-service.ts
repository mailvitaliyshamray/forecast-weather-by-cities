import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { RequesCount, CountRequestService } from './count-request-service';
import { WeatherItems } from '@app/configurations/patterns-service';

const routes = {
  compareWeather: (c: RandomQuoteContext) => 'http://api.openweathermap.org/data/2.5/forecast/daily'
};

export interface RandomQuoteContext {
  category: string;
}

export interface CountContext {
  count: number;
}

@Injectable({
  providedIn: 'root'
})
export class ParametersProcessingService {
  private listCity: any = [];

  constructor(private httpClient: HttpClient, private countRequestService: CountRequestService) {}

  getWeatherSelectedDate(context: RandomQuoteContext, cityName: string, period: string) {
    return this.httpClient
      .cache()
      .get<WeatherItems>(routes.compareWeather(context), {
        params: {
          q: cityName,
          appid: '6cd5eb545fc5819ef89073433e6d66be',
          units: 'metric',
          cnt: period
        },
        observe: 'response'
      })
      .pipe(
        map((body: any) => body),
        catchError(() => of('Client Error Bad Request'))
      );
  }

  getNewNameCityToList(name: string) { 
    localStorage.getItem(name);
  }

  getStatStorage(key: string) { 
    return JSON.parse(localStorage.getItem(key))
  }
  getStateListCities() { 
    return this.listCity.length > 0
  }

  addCityToList(name: string) {
    if (name !== '') {
      if (!this.listCity.includes(name)) {
        this.listCity.push(name);
      }
    }
  }

  clearCityName() { 
    this.listCity = [];
  }

  isNotNameEmpty(key: string, name: string) { 
    if (name.length > 0) {
      this.clearCityName(); 
      this.listCity.push(name);
      localStorage.setItem(key, JSON.stringify(this.listCity));
    }
  }

  removeCityToList(name: string) {
    if (name !== '') {
      if (this.listCity.includes(name)) {
        const index = this.listCity.indexOf(name)
        this.listCity.splice(index, 1);
      }
    }
  }
  
  getCityList(key: any) {
    return JSON.parse(localStorage.getItem(key));
  }
  
  updateCityList(key: string, name: string = '', action: boolean) { 
    if (this.getStatStorage(key) !== null) {
      this.listCity = JSON.parse(localStorage.getItem(key));
      action ? this.addCityToList(name) : this.removeCityToList(name);
      localStorage.setItem(key, JSON.stringify(this.listCity));
    }
    else { 
      this.isNotNameEmpty(key, name)
    }
  }

  getListCity() { 
    return this.listCity
  }

  isExpired(count: number): Observable<RequesCount> {
    const data = {
      reqCount: count
    };
    this.countRequestService.setCurrentNumber(data);
    return of(this.countRequestService.currentNumber);
  }
}
