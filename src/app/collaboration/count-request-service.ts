import { Injectable } from '@angular/core';

export interface RequesCount {
  reqCount: number;
}

const countKey = 'countKey';

@Injectable({
  providedIn: 'root'
})
export class CountRequestService {
  private _requestCount: RequesCount | null = null;

  constructor() {
    sessionStorage.setItem(countKey, JSON.stringify(1));
    this.getCurrentNumber();
  }

  get currentNumber(): RequesCount | null {
    return this._requestCount;
  }

  getCurrentNumber(): any {
    this._requestCount = Object.assign({}, { reqCount: Number(sessionStorage.getItem(countKey)) });
  }

  addNewNumber(): void {
    sessionStorage.setItem(countKey, JSON.stringify(this.currentNumber.reqCount));
  }

  setCurrentNumber(nextReqest: RequesCount) {
    this.getCurrentNumber();
    this.currentNumber.reqCount += nextReqest.reqCount;
    if (nextReqest) {
      this.addNewNumber();
    } else {
      sessionStorage.removeItem(countKey);
    }
  }
}
