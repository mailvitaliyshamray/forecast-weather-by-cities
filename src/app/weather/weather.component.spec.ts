import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarComponent } from '@app/calendar/calendar.component';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { CoreModule } from '@app/core';
import { WeatherComponent } from './weather.component';
import { CitiesComponent } from '../cities/cities.component';
import { Router } from '@angular/router';

describe('ExchangeComponent', () => {
  let vm: WeatherComponent;
  let fixture: ComponentFixture<WeatherComponent>;
  let router: Router;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        NgbModule, MatDatepickerModule, MatNativeDateModule,
        RouterTestingModule, ReactiveFormsModule, CoreModule],
      declarations: [WeatherComponent, CalendarComponent],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();
    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherComponent);
    vm = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(vm).toBeTruthy();
  });

  it('Should addCityToLis set data to aray listCity ', () => {
    // Setup
    const city = 'Kiev';
    const key = 'keyCitiesList';
    const data = [city]
    const EXPECTEDRESULT = [city];

    // Act
    localStorage.setItem(key, JSON.stringify(data));
    vm.addCityToList(city)

    // Assert
    expect(EXPECTEDRESULT).toEqual(vm.listCity);
  })

  it('Should toAddCity calls router.navigate with param', () => {
    // Setup
    const param = ['/cities'];
    jest.spyOn(router, 'navigate');

    // Act
    vm.toAddCity();

    // Assert
    expect(router.navigate).toHaveBeenCalledTimes(1);
    expect(router.navigate).toHaveBeenCalledWith(param);
  })

  it('Should weather calls updateButtonName method', () => {
    // Setup
    vm.updateButtonName = jest.fn();

    // Act
    vm.weather();

    // Assert
    expect(vm.updateButtonName).toHaveBeenCalledTimes(1);
    expect(vm.updateButtonName).toHaveBeenCalled();
  })
});
