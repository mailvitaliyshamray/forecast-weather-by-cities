import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherComponent } from './weather.component';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { CalendarComponent } from '../calendar/calendar.component';
  
@NgModule({
  imports: [CommonModule, MatNativeDateModule, MatDatepickerModule,
    ReactiveFormsModule, NgbModule, WeatherRoutingModule],
  declarations: [WeatherComponent, CalendarComponent]
})
export class WeatherModule {}
