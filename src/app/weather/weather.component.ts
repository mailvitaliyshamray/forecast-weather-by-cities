import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '@env/environment';
import { delay } from 'rxjs/operators';
import { SelectedDailyProps, PatternsService } from '@app/configurations/patterns-service';
import { RequestHandingService } from '@app/collaboration/request-handing-service';
import { ParametersProcessingService } from '@app/collaboration/parameters-processing-service';
import { finalize } from 'rxjs/operators';
import { CalendarComponent } from '../calendar/calendar.component';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, OnDestroy {
  quote: string | undefined;
  version: string | null = environment.version;
  error: string | undefined;
  weatherForm!: FormGroup;
  isLoading = false;
  countRequest: number;
  listCity: any = [];
  defaultButtonName: string;
  buttonName: string;
  addCitybuttonName: string;
  article: string;
  dailyWeatherProperties: Array<SelectedDailyProps>;
  showCurrentDate: string;
  myCalendar: CalendarComponent;
  keyCities: string;
  newCity: string;
  _currentDate: string;
  
  constructor(
    private requestHandingService: RequestHandingService,
    private parametersProcessingService: ParametersProcessingService,
    private patternsService: PatternsService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }
  
  @ViewChild('tcalendar', { static: false })
  
  ngOnInit() {
    this.newCity = this.route.snapshot.queryParams.cityName;
    this.countRequest = 1;
    this.keyCities = 'keyCitiesList';
    this.defaultButtonName = 'show weather';
    this.article = ' at the ';
    this.buttonName = this.defaultButtonName;
    this.addCitybuttonName = 'editing cities list';
    this.getCurrentDate();
    this.showWeatherOfNewCity(this.newCity);
    this.addCityToList();
  }
    
  ngOnDestroy() {}

  getCurrentDate(): void { 
    this.showCurrentDate = moment().format('YYYY-MM-DD');
  }

  get showForeCast(): boolean {
    return this.dailyWeatherProperties !== undefined ? this.dailyWeatherProperties.length > 0 : false;
  }

  get showCity(): string {
    return this.cityName();
  }

  addCityToList(city?: string) {
    this.listCity = JSON.parse(localStorage.getItem(this.keyCities));
  }

  showWeatherOfNewCity(newCity: string) { 
    if (newCity !== undefined) { 
      this.newCity = newCity;
      this.weatherForm.value.city = newCity;
      this.cityName();
      this.clearDeilyWeatherProperty();
      this.getForeCastByCity(newCity);
    }
  }

  updateButtonName() {
    this.buttonName = this.combainButtonName();
  }

  onChangeObj(event: any) {
    this.clearDeilyWeatherProperty();
    this.updateButtonName();
  }

  isButtonActive(nextReques: any) {
    return nextReques.reqCount > 5;
  }

  cityName() {
    const name = this.weatherForm.value;
    return name.city === 'Choose City' ? '' : name.city;
  }

  combainButtonName() {
    const name = this.cityName();
    return name.length > 0 ? this.defaultButtonName + this.article + name : this.defaultButtonName;
  }

  isNotEmpty(data: any) {
    return data.length > 0;
  }

  clearDeilyWeatherProperty() {
    this.dailyWeatherProperties = [];
  }

  getSelectedData(cityName: string, cnt: number) {
    const period = cnt.toString();
    if (this.isNotEmpty(cityName)) {
      this.parametersProcessingService
        .getWeatherSelectedDate({ category: 'compareWeather' }, cityName, period )
        .pipe(
          delay(1000),
          finalize(() => {
            this.clearDeilyWeatherProperty();
            this.dailyWeatherProperties = this.patternsService.getDailyWeatherProperties();
            this.isLoading = false;
          })
        )
        .subscribe((data) => {
          if (data === 'Client Error Bad Request') {
            this.router.navigate(['/error']);
          }
          else { 
            this.weatherForm.value.city = data.body.city.name
            this.patternsService.separateDailyWeatherData(data.body);
          }
        });
    }
  }

  datesCompare(timestamp: number): number {
    const numberCurrentDate = this.getNumberCurrentDate();
    const numberSelectedDate = moment.unix(timestamp).diff(moment().date(), 'days');
    return (numberSelectedDate - numberCurrentDate) + 1
  }

  getNumberCurrentDate(): number {
    const formatCurrentToday = moment().format('YYYY-MM-DD');
    return moment.unix(moment(formatCurrentToday).unix()).diff(moment().date(), 'days');
  }

  getSelectedDayTimestamp(selectedDay: Moment): number {
    const formatSelectedDay = moment(selectedDay).format('YYYY-MM-DD HH:mm:SS');
    return moment(formatSelectedDay).unix();
  }

  dateSelected(selectedDay: Moment) {
    const timestamp = this.getSelectedDayTimestamp(selectedDay);
    const cnt = this.datesCompare(timestamp);
    this.getSelectedData(this.cityName(), cnt)
  }

  getForeCastByCity(cityName: string) {
    const today = 1;
    this.getSelectedData(cityName, today);
  }

  weather() {
    this.updateButtonName();
    this.requestHandingService.getCountrequest();
    this.getForeCastByCity(this.cityName());
  }

  toAddCity() {
    this.router.navigate(['/cities']);
  }

  private createForm() {
    this.weatherForm = this.formBuilder.group({
      city: ['']
    });
  }
}
