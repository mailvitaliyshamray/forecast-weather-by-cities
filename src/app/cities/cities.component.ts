
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ParametersProcessingService } from '@app/collaboration/parameters-processing-service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent implements OnInit, OnDestroy {
  addCityForm!: FormGroup;
  removeCityForm!: FormGroup;
  city: string;
  keyCities: string;
  listCity: any[];
  
  constructor(
    private router: Router,
    private formAddBuilder: FormBuilder,
    private formRemoveBuilder: FormBuilder,
    private parametersProcessingService: ParametersProcessingService
  ) {
    this.createForm();
    this.createRemoveCityForm();
  }

  ngOnInit() {
    this.city = '';
    this.keyCities = 'keyCitiesList';
    this.listCity = this.parametersProcessingService.getCityList(this.keyCities);
  }
    
  ngOnDestroy() { }
  
  onChangeList(event: any) { }

  async removeCity() { 
    const action = false;
    const city = this.removeCityForm.value.remCity;
    await this.parametersProcessingService.updateCityList(this.keyCities, city, action);
    this.router.navigate(['/weather'], {
      queryParams: { cityName: '' },
      replaceUrl: true
    });
  }

  async addCity() { 
    const action = true;
    this.city = this.addCityForm.value.addCity;
    await this.parametersProcessingService.updateCityList(this.keyCities, this.city, action);
    this.router.navigate(['/weather'], {
      queryParams: { cityName: this.city },
      replaceUrl: true
    });
  }
  
  private createForm() {
    this.addCityForm = this.formAddBuilder.group({
      addCity: ['', Validators.required]
    });
  }

  private createRemoveCityForm() {
    this.removeCityForm = this.formRemoveBuilder.group({
      remCity: ['', Validators.required]
    });
  }
 
}
