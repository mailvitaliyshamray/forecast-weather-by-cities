import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WeatherModule } from '../weather/weather.module';
import { CitiesRoutingModule } from './cities-routing.module';
import { CitiesComponent } from './cities.component';
  
@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgbModule, CitiesRoutingModule, WeatherModule],
  declarations: [CitiesComponent, ]
})
export class CitiesModule {}
